import DOMPurify from "isomorphic-dompurify";
import parse from "html-react-parser";
// import * as dayjs from "dayjs";
// import "dayjs/locale/it";

export function renderHTML(dirt) {
  const clean = DOMPurify.sanitize(dirt, { USE_PROFILES: { html: true } });
  return parse(clean);
}

export function formatDate(str, locale) {
  const fmt = "DD MMMM YYYY";
  return dayjs(str).locale(locale).format(fmt);
}
