const imgFrag = `
  fragment imgFrag on ResponsiveImage {
    aspectRatio
    base64
    height
    sizes
    src
    srcSet
    webpSrcSet
    width
    alt
    title
  }
`;

const txtImgBlock = `
  model: _modelApiKey
  id
  bg
  img {
    responsiveImage(sizes: "(min-width: 768px) 50vw, 100vw, 450px", imgixParams: {auto: [format, compress], fit: crop, ar: "5:4"}){
      ...imgFrag
    }
  }
  internalLink {
    id
    link {
      slug
      title
      id
    }
    callToAction
  }
  prefix
  title
  txt
`;

const imgBlock = `
  model: _modelApiKey
  id
  img {
    responsiveImage(sizes: "(min-width: 768px) 70vw, 0vw, 950px", imgixParams: {auto: [format, compress], fit: crop, ar: "16:9"}){
      ...imgFrag
    }
  }
`;

const txtBlock = `
  id
  model: _modelApiKey
  txt
`;

const bloccoVideo = `
id
model: _modelApiKey
`;

const internalLink = `
id
model: _modelApiKey
link
`;

const seoBlock = `
tag
attributes
content
`;

export const getHomepage = `
query homepage {
  homepage {
    titleHero
    title
    id
    prefixIntro
    imgPng {
      responsiveImage(sizes: "(min-width: 768px) 50vw, 100vw, 450px", imgixParams: {auto: [format, compress]}){
        ...imgFrag
      }
    }
    slug
    subtitleHero
    title
    titleHero
    titleIntro
    txtBig
    txtIntro
    txtSmall
    content {
      ${txtImgBlock}
    }
    contenBis {
      ${txtImgBlock}
    }
    seo: _seoMetaTags {
      ${seoBlock}
    }
  }
  allServices {
    id
    title
    createdAt
    abstract
  }
}
${imgFrag}
`;

export const getAllPages = `
query allPages {
  allPages {
    slug
    model: _modelApiKey
  }
}
`;

export const site = `
query site {
  site: _site{
    favicon: faviconMetaTags {
      tag
      content
      attributes
    }
  }
}
`;

export const getPage = `
query page($slug: String!) {
  page(filter: {slug: {eq: $slug}}) {
    id
    intro {
      ${txtImgBlock}
    }
    section {
      titleSection
      id
      slug
      model: _modelApiKey
      content {
        ... on BloccoVideoRecord {
          ${bloccoVideo}
        }
        ... on ImgBlockRecord {
          ${imgBlock}
        }
        ... on TxtBlockRecord {
          ${txtBlock}
        }
      }
    }
    slug
    subtitleHero
    title
    titleHero
    seo: _seoMetaTags {
      ${seoBlock}
    }
  }
  services: allServices {
    id
    title
    content {
      ... on BloccoVideoRecord {
        ${bloccoVideo}
      }
      ... on ImgBlockRecord {
        ${imgBlock}
      }
      ... on TxtBlockRecord {
        ${txtBlock}
      }
    }
  }
}
${imgFrag}
`;
