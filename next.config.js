module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: ["it"],
    defaultLocale: "it",
  },
  optimizeCss: {
    inlineFonts: true,
    preloadFonts: true,
    logLevel: "error",
  },
};
