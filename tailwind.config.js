module.exports = {
  mode: "jit",
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        black: "#141213",
        blue: {
          DEFAULT: "#1479FF",
          light: "#E7F2FF",
          dark: "#1479FF",
        },
      },
      fontSize: {
        xxs: ["10px", "20px"],
        xs: ["12px", "22px"],
        sm: ["16px", "24px"],
        base: ["18px", "28px"],
        lg: ["20px", "32px"],
        xl: ["24px", "37px"],
        "2xl": ["32px", "44px"],
        "3xl": ["48px", "61px"],
        "4xl": ["71px", "76px"],
      },
    },
  },
  plugins: [],
};
