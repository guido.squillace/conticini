import { renderHTML } from "lib/utils";

export default function TxtBlock({ record }) {
  return <div className="text-sm lg:text-base">{renderHTML(record.txt)}</div>;
}
