import PostContent from "components/PostContent";
import Link from "next/link";

export default function Section({ record }) {
  return (
    <>
      <div className="grid gap-3 py-6 lg:grid-cols-4 lg:gap-6 lg:py-12">
        <div className="hidden py-2 lg:block">
          <div className="sticky top-6">
            <Link href={`#${record.slug}`}>
              <a className="anchor">{record.titleSection}</a>
            </Link>
          </div>
        </div>
        <div id={record.slug} className="grid gap-3 lg:col-span-3 lg:gap-4">
          <div className="title--small text-blue">{record.titleSection}</div>
          {record.content.map((block) => (
            <PostContent key="id" record={block} />
          ))}
        </div>
      </div>
    </>
  );
}
