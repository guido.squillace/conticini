import { renderHTML } from "lib/utils";
import { Image as DatoImage } from "react-datocms";
import Link from "next/link";

export default function TxtImgBlock({ record }) {
  return (
    <>
      <section
        className={`${record.bg == true ? "bg-white" : "bg-blue-light"}`}
      >
        <div className="container">
          <div className="space--standard">
            <div className="grid gap-3 lg:grid-cols-2 lg:gap-6">
              <div className="grid gap-3 lg:content-center lg:gap-6 lg:pr-24">
                {record.prefix && <div className="prefix">{record.prefix}</div>}
                <h2 className="title text-blue">{record.title}</h2>
                {record.txt && (
                  <div className="text">{renderHTML(record.txt)}</div>
                )}
                {record.internalLink.length > 0 &&
                  record.internalLink.map((block) => (
                    <div key={block.id} className="">
                      <Link href={`/${block.link}`}>
                        <a title={block.title} className="button">
                          {block.callToAction}
                        </a>
                      </Link>
                    </div>
                  ))}
              </div>
              <div className="mt-2">
                <DatoImage
                  className=""
                  data={record.img.responsiveImage}
                  alt={record.img.alt}
                  title={record.img.title}
                  layout=""
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
