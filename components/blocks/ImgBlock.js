import { Image as DatoImage } from "react-datocms";

export default function ImgBlock({ record }) {
  const img = record.img;
  return (
    <DatoImage
      className="my-4"
      data={img.responsiveImage}
      alt={img.responsiveImage.alt}
      title={img.responsiveImage.title}
      layout=""
    />
  );
}
