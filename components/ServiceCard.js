export default function ServiceCard({ record, i }) {
  return (
    <>
      <div className="rounded-lg bg-blue-light p-5 text-black lg:p-10">
        <div className="flex gap-x-4">
          <div className="text-3xl font-semibold text-blue lg:text-4xl">
            {i}.
          </div>
          <h2 className="pt-2 text-lg font-semibold lg:text-xl">
            {record.title}
          </h2>
        </div>
        <div className="mt-3 lg:text-base">{record.abstract}</div>
      </div>
    </>
  );
}
