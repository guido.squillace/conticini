import { useRouter } from "next/router";
import Link from "next/link";
import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import { PhoneIcon } from "@heroicons/react/solid";

const resources = [
  {
    name: "Dr. Conticini",
    href: "/edoardo-conticini-dottore-in-reumatologia",
  },
  {
    name: "La reumatologia",
    href: "/la-reumatologia",
  },
  {
    name: "La diagnosi",
    href: "/la-diagnosi",
  },
  {
    name: "Servizi",
    href: "/servizi",
  },
  {
    name: "Contatti",
    href: "/contatti",
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

function Header(props) {
  const router = useRouter();

  return (
    <header>
      <Popover className="relative border-b border-black/5 bg-white">
        <div className="container px-4 sm:px-6 xl:px-0">
          <div className="flex items-center justify-between border-gray-100 py-3 md:justify-between md:space-x-10">
            <div className="flex justify-start">
              <Link href="/">
                <a title="Homepage Edoardo Conticini" className="">
                  <div className="w-32 text-lg font-semibold lg:w-60 lg:text-2xl">
                    Dr Edoardo Conticini
                  </div>
                </a>
              </Link>
            </div>
            <div className="-my-2 -mr-2 md:hidden">
              <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span className="sr-only">Open menu</span>
                <MenuIcon className="h-8 w-8 text-black" aria-hidden="true" />
              </Popover.Button>
            </div>
            <Popover.Group
              as="nav"
              className="hidden justify-end space-x-6 md:flex lg:space-x-12"
            >
              {resources.map((item, i) => (
                <Link key={i} href={item.href}>
                  <a
                    title={item.name}
                    className={`${
                      router.asPath == item.href ? "text-blue" : "text-black"
                    } whitespace-nowrap text-xs font-semibold duration-300 hover:text-blue lg:text-base`}
                  >
                    {item.name}
                    {console.log("router:", router.asPath)}
                  </a>
                </Link>
              ))}
            </Popover.Group>
          </div>
        </div>

        <Transition
          as={Fragment}
          enter="duration-200 ease-out"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="duration-100 ease-in"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95"
        >
          <Popover.Panel
            focus
            className="absolute inset-x-0 top-0 z-50 origin-top-right transform bg-blue text-white transition md:hidden"
          >
            <div className="divide-y-2 divide-gray-50 bg-blue text-white shadow-lg ring-1 ring-black ring-opacity-5">
              <div className="px-5 py-3">
                <div className="flex items-center justify-between">
                  <div>
                    <Link href="/">
                      <a
                        title="Homepage Edoardo Conticini"
                        className="w-32 text-lg font-semibold	text-white"
                      >
                        <div>Dr Edoardo Conticini</div>
                      </a>
                    </Link>
                  </div>
                  <div className="-mr-2">
                    <Popover.Button className="inline-flex items-center justify-center rounded-md p-2 text-white">
                      <span className="sr-only">Close menu</span>
                      <XIcon className="h-8 w-8" aria-hidden="true" />
                    </Popover.Button>
                  </div>
                </div>
              </div>
              <div className="space-y-6 py-6 px-5">
                <div className="grid gap-y-4 gap-x-8">
                  {resources.map((item, i) => (
                    <Link key={i} href={item.href}>
                      <a
                        title={item.name}
                        className="text-xl font-medium text-white"
                      >
                        {item.name}
                      </a>
                    </Link>
                  ))}
                </div>
                <div>
                  <Link href="tel:+393333333333">
                    <a
                      title=""
                      className="flex w-full items-center rounded-lg bg-white px-4 py-2 text-xl font-medium text-black"
                    >
                      <PhoneIcon
                        className="mr-4 h-7 w-7 text-blue"
                        aria-hidden="true"
                      />
                      +39 333 33 33 333
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </header>
  );
}

export default Header;
