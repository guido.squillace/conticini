import { Fragment } from "react";
import Link from "next/link";
import Image from "next/image";

export default function Footer({ data }) {
  const year = new Date().getFullYear();
  return (
    <>
      {data.slug == "contact" ? (
        <section className="bg-blue-light">
          <div className="container">
            <div className="space--standard">
              <div className="grid gap-3 lg:grid-cols-3 lg:gap-6">
                <div className="grid gap-3 lg:col-span-2 lg:content-center lg:gap-6 lg:pr-24">
                  <div className="prefix">Prenota una visita</div>
                  <h2 className="title text-blue">
                    Chiedi una visita, o scrivimi per qualsiasi dubbio o
                    chiarimento
                  </h2>
                </div>
                <div className="lg:grid lg:place-content-end">
                  <Link href="/contatti#form">
                    <a
                      className="button"
                      title="Vai al form di contatto del sito"
                    >
                      Contattami
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>
      ) : null}
      <footer id="footer" data-datocms-noindex className="container py-4">
        <div className="grid gap-4 py-4 xl:grid-cols-3">
          <div className="xl:col-span-2 xl:flex xl:gap-6">
            <div className="text-gray text-xs">
              <span>@ {year} Dr Edoardo Conticini</span>
              <span className="px-1"> - </span>
              <span>P.IVA 840582367</span>
              <span className="px-1"> - </span>
              <Link href="https://www.guidosquillace.it">
                <a
                  title="Guido Squillace web developer frontend"
                  className="hover:text-yellow"
                  target="_blank"
                  rel="noreferrer"
                >
                  Design & dev Guido Squillace
                </a>
              </Link>
            </div>
          </div>

          <div className="text-gray text-xs uppercase xl:text-right">
            <Link href="https://www.iubenda.com/privacy-policy/#####">
              <a
                title="Privacy Policy"
                className="iubenda-nostyle no-brand iubenda-embed hover:text-yellow"
                target="_blank"
                rel="noreferrer"
              >
                Privacy Policy
              </a>
            </Link>
            <span className="px-1"> - </span>
            <Link href="https://www.iubenda.com/privacy-policy/#####">
              <a
                title="Cookie Policy"
                className="iubenda-nostyle no-brand iubenda-embed hover:text-yellow"
                target="_blank"
                rel="noreferrer"
              >
                Cookie Policy
              </a>
            </Link>
          </div>
        </div>
      </footer>
    </>
  );
}
