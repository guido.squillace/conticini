import React from "react";
import { Formik, Form, useField } from "formik";
import Link from "next/link";
import * as Yup from "yup";
import { useRouter } from "next/router";

const WEB3FORMS_KEY = process.env.NEXT_PUBLIC_W3F;
const COOKIE_TOKEN = process.env.NEXT_PUBLIC_IUBENDA_SITE_ID;
const SITE_URL = process.env.SITE_URL;

const MyTextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <div>
      <label className="form__label" htmlFor={props.id || props.name}>
        {label}
      </label>
      <input
        className={`${
          meta.touched && meta.error
            ? "form__input input__error"
            : "form__input"
        }`}
        {...field}
        {...props}
      />
      {meta.touched && meta.error ? (
        <div className="form__error">{meta.error}</div>
      ) : null}
    </div>
  );
};

const MyTextarea = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div>
      <label className="form__label" htmlFor={props.id || props.name}>
        {label}
      </label>
      <textarea
        rows="4"
        className={`${
          meta.touched && meta.error
            ? "form__input input__error"
            : "form__input"
        }`}
        {...field}
        {...props}
      />
      {meta.touched && meta.error ? (
        <div className="form__error">{meta.error}</div>
      ) : null}
    </div>
  );
};

const MyCheckbox = ({ children, ...props }) => {
  const [field, meta] = useField({ ...props, type: "checkbox" });
  return (
    <div>
      <label className="form__label__checkbox flex">
        <input
          type="checkbox"
          className={`${
            meta.touched && meta.error
              ? "form__input__checkbox input__error"
              : "form__input__checkbox"
          }`}
          {...field}
          {...props}
        />
        {children}
      </label>
      {meta.touched && meta.error ? (
        <div className="form__error">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default function FormComponent() {
  async function handleSubmit(formValues) {
    console.log("formValues", formValues);
    const response = await fetch("https://api.web3forms.com/submit", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        apikey: WEB3FORMS_KEY,
        subject: "Contatto dal sito",
        from_name: "edordoconticini.it",
        Nome: formValues.name,
        Telefono: formValues.phone,
        Dove: formValues.dove,
        Email: formValues.email,
        Messaggio: formValues.messagge,
      }),
    });
    const result = await response.json();
    if (result.success) {
      console.log(result);
    }
  }

  const router = useRouter();
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  return (
    <>
      <div className="">
        <Formik
          initialValues={{
            name: "",
            phone: "",
            email: "",
            dove: "",
            messagge: "",
            acceptedTerms: true,
          }}
          validationSchema={Yup.object({
            name: Yup.string().required("Campo richiesto"),
            phone: Yup.string()
              .matches(phoneRegExp, "phoneNoValid")
              .required("fieldRequest"),
            email: Yup.string()
              .email("Formato dell'email non è corretto")
              .required("Campo richiesto"),
            dove: Yup.string(),
            messagge: Yup.string(),
            acceptedTerms: Yup.boolean()
              .required("Campo richiesto")
              .oneOf([true], "Devi accettare termini e condizioni"),
          })}
          onSubmit={(formValues) => {
            handleSubmit(formValues);
            router.push("https://web3forms.com/success");
          }}
        >
          <Form>
            <div className="grid gap-1 gap-x-3 p-1 md:grid-cols-2 lg:gap-3 lg:p-5 2xl:gap-x-6 2xl:gap-y-4">
              <div className="md:col-span-2">
                <div className="title">Richiedi una prenotazione online</div>
                <div className="title--small mb-4 text-blue">
                  compila il form:
                </div>
              </div>
              <MyTextInput
                label="Nome & Cognome *"
                name="name"
                id="name"
                type="text"
                placeholder="Inserisci Nome e Cognome"
              />
              <MyTextInput
                label="Email *"
                name="email"
                id="email"
                type="email"
                placeholder="Inserisci indirizzo email"
              />
              <MyTextInput
                label="Numero di telefono *"
                name="phone"
                id="phone"
                type="number"
                placeholder="Inserisci numero di telefono"
              />
              <MyTextInput
                label="Dove vuoi essere visitato?"
                name="dove"
                id="dove"
                type="text"
                placeholder="Inserisci luogo della visita"
              />
              <div className="md:col-span-2">
                <MyTextarea
                  label="Note"
                  name="messagge"
                  id="messagge"
                  type="text"
                  placeholder="inserisci il messaggio"
                />
              </div>
              <div className="md:col-span-2">
                <MyCheckbox name="acceptedTerms">
                  <span className="mt-1 block pl-2 text-xxs">
                    Ho letto e accetto la{" "}
                    <Link
                      href={`//www.iubenda.com/privacy-policy/${COOKIE_TOKEN}`}
                    >
                      <a
                        target="_blank"
                        className="iubenda-nostyle no-brand iubenda-embed mx-1"
                      >
                        Privacy Policy
                      </a>
                    </Link>
                    according to UE 2016/679 (GDPR)
                  </span>
                </MyCheckbox>
              </div>
              <div className="mt-2 md:col-span-2">
                <button className="group block w-full" type="submit">
                  <span className="button">Invia la richiesta</span>
                </button>
              </div>
            </div>
          </Form>
        </Formik>
      </div>
    </>
  );
}
