import TxtImgBlock from "./blocks/TxtImgBlock.js";
import TxtBlock from "./blocks/TxtBlock.js";
import ImgBlock from "./blocks/ImgBlock.js";
import InternalLink from "./blocks/InternalLink.js";
import Section from "./blocks/Section.js";
import BloccoVideo from "./blocks/BloccoVideo.js";

export default function PostContent({ record }) {
  switch (record.model) {
    case "txt_img_block":
      return <TxtImgBlock record={record} />;
    case "txt_block":
      return <TxtBlock record={record} />;
    case "img_block":
      return <ImgBlock record={record} />;
    case "blocco_video":
      return <BloccoVideo record={record} />;
    case "internal_link":
      return <InternalLink record={record} />;
    case "section":
      return <Section record={record} />;
  }
}
