import { Image as DatoImage } from "react-datocms";
import Link from "next/link";

export default function HeroHp({ record }) {
  return (
    <header className="bg-gradient-to-t from-blue-light">
      <div className="container pt-4">
        <div className="grid gap-4 lg:grid-cols-12">
          <div className="grid gap-3 lg:col-span-7 lg:content-center lg:gap-10 lg:pr-48">
            <h2 className="title--big">
              {record.titleHero} <span className="text-blue">reumatologia</span>
            </h2>
            <div className="text">{record.subtitleHero}</div>
            <div className="">
              <Link href="/contatti#prenota">
                <a title="Prenota un appuntamento online" className="button">
                  Prenota un appuntamento online
                </a>
              </Link>
            </div>
          </div>
          <div className="lg:col-span-5">
            <DatoImage
              className="h-auto w-full"
              data={record.imgPng.responsiveImage}
              alt={record.imgPng.alt}
              title={record.imgPng.title}
              placeholde="no-placeholder"
              // layout="fill"
            />
          </div>
        </div>
      </div>
    </header>
  );
}
