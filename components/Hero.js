export default function Hero({ data }) {
  return (
    <header className="bg-gradient-to-t from-blue-light">
      <div className="container">
        <div className="grid gap-1 pt-3 pb-10 lg:pt-24 lg:pb-32 lg:pl-80">
          <h1 className="title--big pb-1 text-blue lg:pb-3">
            {data.titleHero}
          </h1>
          <h2 className="text">{data.subtitleHero}</h2>
        </div>
      </div>
    </header>
  );
}
