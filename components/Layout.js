import Header from "./Header";
import Footer from "./Footer";

function Layout({ children, data }) {
  return (
    <>
      <Header />
      <main id="content">{children}</main>
      <Footer data={data} id="footer" />
    </>
  );
}

export default Layout;
