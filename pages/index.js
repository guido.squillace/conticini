import Head from "next/head";
import { renderMetaTags } from "react-datocms";
import Link from "next/link";

import Layout from "components/Layout";
import * as queries from "lib/queries";
import fetchDato from "lib/dato";
import HeroHp from "components/HeroHp";
import PostContent from "components/PostContent";
import { renderHTML } from "lib/utils";
import ServiceCard from "components/ServiceCard";

function Home({ home, data, services }) {
  const { seo } = home;
  const { site } = data;
  return (
    <Layout data={home}>
      <Head>{renderMetaTags(seo.concat(site.favicon))}</Head>
      <h1 className="sr-only">Homepage | Le Anfore tenuta Casadei</h1>
      <HeroHp record={home} />
      <section className="space--standard bg-white">
        <div className="container">
          <div className="grid gap-3 lg:grid-cols-2 lg:gap-6">
            <h2 className="prefix lg:col-span-2 lg:mb-6">{home.prefixIntro}</h2>
            <h3 className="title">{home.titleIntro}</h3>
            <div className="grid gap-3 lg:gap-6">
              <div className="text lg:mb-8">{renderHTML(home.txtIntro)}</div>
              <div>
                <Link href="/edoardo-conticini-dottore-in-reumatologia#curriculum-vitae">
                  <a
                    title="Curriculum vitae di Edoardo Conticini"
                    className="button"
                  >
                    Formazione professionale
                  </a>
                </Link>
              </div>
              <div>
                <Link href="/edoardo-conticini-dottore-in-reumatologia#ricerca">
                  <a
                    title="Tutte le pubblicazioni di Edoardo Conticini"
                    className="button"
                  >
                    La ricerca
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      {home.content.map((block) => (
        <PostContent key="id" record={block} />
      ))}
      <section className="space--standard">
        <div className="container">
          <div className="grid gap-3 lg:grid-cols-2 lg:gap-6">
            <h2 className="title text-blue lg:col-span-2">Servizi</h2>
            <h3 className="text-base lg:pr-24 lg:text-2xl">{home.txtBig}</h3>
            <div className="text">{renderHTML(home.txtSmall)}</div>
            <div className="">
              <Link href="/servizi">
                <a title="Servizi" className="button">
                  Scropri tutti i servizi
                </a>
              </Link>
            </div>
          </div>
          <div className="mt-6 grid gap-4 md:grid-cols-2 lg:mt-12 lg:grid-cols-3 lg:gap-6">
            {services.map((service, i) => (
              <ServiceCard i={i + 1} record={service} key={service.id} />
            ))}
          </div>
        </div>
      </section>
      {home.contenBis.map((block) => (
        <PostContent key="id" record={block} />
      ))}
    </Layout>
  );
}

export async function getStaticProps() {
  const response = await fetchDato(queries.getHomepage);
  const data = await fetchDato(queries.site);
  return {
    props: {
      home: response.homepage,
      services: response.allServices,
      data,
    },
  };
}

export default Home;
