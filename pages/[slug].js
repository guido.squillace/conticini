import Head from "next/head";
import { renderMetaTags } from "react-datocms";

import Form from "components/Form";
import PostContent from "components/PostContent";
import Hero from "components/Hero";
import Layout from "components/Layout";
import * as queries from "lib/queries";
import fetchDato from "lib/dato";

function Page({ data, page, services }) {
  return (
    <Layout data={page}>
      <Head>{renderMetaTags(page.seo.concat(data.site.favicon))}</Head>
      <Hero data={page} />
      {page.intro.map((block) => (
        <div className="" key={block.id}>
          <PostContent record={block} />
        </div>
      ))}
      {page.slug == "servizi" ? (
        <>
          <div className="container grid gap-3 bg-white py-6 md:gap-12 lg:py-24">
            {services.map((s, i) => (
              <div
                key={s.id}
                className="rounded-lg bg-blue-light p-5 text-black lg:p-16"
              >
                <div className="flex gap-x-4">
                  <div className="text-3xl font-semibold text-blue lg:text-4xl">
                    {i + 1}.
                  </div>
                  <h2 className="pt-2 text-lg font-semibold lg:text-2xl">
                    {s.title}
                  </h2>
                </div>
                <div className="mt-3 lg:text-base">
                  {s.content.map((block) => (
                    <div className="" key={block.id}>
                      <PostContent record={block} />
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </>
      ) : page.slug == "contatti" ? (
        <div id="prenota" className="container py-6 lg:py-24 lg:px-40">
          <Form />
        </div>
      ) : (
        <section className="lg-py-12 border-t border-black/5 py-6">
          <div className="container">
            {page.section.length > 0 ? (
              <>
                {page.section.map((block) => (
                  <div className="" key={block.id}>
                    <PostContent record={block} />
                  </div>
                ))}
              </>
            ) : null}
          </div>
        </section>
      )}
    </Layout>
  );
}

export async function getStaticPaths() {
  const response = await fetchDato(queries.getAllPages);
  const paths = response.allPages.map(({ slug }) => ({
    params: { slug },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  const { slug } = params;
  const response = await fetchDato(queries.getPage, { slug });
  const data = await fetchDato(queries.site);
  return {
    props: {
      page: response.page,
      services: response.services,
      data,
    },
  };
}

export default Page;
